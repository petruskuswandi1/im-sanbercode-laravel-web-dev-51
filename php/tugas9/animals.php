<?php
class Animals
{
    public $name;
    private $legs = 4;
    protected $cold_blooded = "no";

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function setLegs($legs)
    {
        $this->legs = $legs;
    }

    public function getColdBlooded()
    {
        return $this->cold_blooded;
    }
}
