<?php
require('animals.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animals("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->getLegs() . "<br>";
echo "cold_blooded : " . $sheep->getColdBlooded() . "<br>";

echo "<br>";

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->getLegs() . "<br>";
echo "cold_blooded : " . $kodok->getColdBlooded() . "<br>";
$kodok->jump();

echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->setLegs(2);

echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->getLegs() . "<br>";
echo "cold_blooded : " . $sungokong->getColdBlooded() . "<br>";
$sungokong->yell();
