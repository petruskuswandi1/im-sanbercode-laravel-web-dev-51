<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function daftar()
    {
        return view('page.daftar');
    }

    public function welcome(Request $req)
    {
        $nama_depan = $req->input('fname');
        $nama_belakang = $req->input('lname');

        return view('page.home', ["nama_depan" => $nama_depan, "nama_belakang" => $nama_belakang]);
    }
}
