<?php

use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'home']);
Route::get('/daftar', [BiodataController::class, 'daftar']);

Route::post('/home', [BiodataController::class, 'welcome']);

Route::get('/data-table', function () {
    return view('page.datatable');
});

Route::get('/table', function () {
    return view('page.table');
});

// CRUD Categories
// C => Create Data
// route untuk mengarah ke form inputan tambah Category
Route::get('/category/create', [CategoryController::class, 'create']);
// Route simpan data inputan ke DB table category
Route::post('/category', [CategoryController::class, 'store']);

// R => Read Data
// untuk menampilkan semua data pada table category masuk tampil ke WB
Route::get('/category', [CategoryController::class, 'index']);
// untuk menampilkan data berdasarkan parameter id
Route::get('/category/{id}', [CategoryController::class, 'show']);
// U => Update Data
// route yang mengarah ke form update inputan category berdasarkan id
Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);
// untuk update data ke DB table categories berdasarkan id
Route::put('category/{id}', [CategoryController::class, 'update']);

// D => Delete Data
Route::delete('/category/{id}', [CategoryController::class, 'destroy']);
