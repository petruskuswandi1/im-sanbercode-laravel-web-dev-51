@extends('layouts.master')
@section('title')
    Halaman Daftar
@endsection
@section('content')
    <h1>Halaman Daftar</h1>
    <form action="/home" method="post">
        @csrf
        <label for="fname">Nama Depan</label><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label for="lname">Nama Belakang</label><br>
        <input type="text" name="lname" id="lname"><br><br>
        <input type="submit" value="Kirim">
    </form>
@endsection
