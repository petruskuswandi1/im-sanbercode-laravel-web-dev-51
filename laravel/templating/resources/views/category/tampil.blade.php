@extends('layouts.master')
@section('title')
    Halaman Tampil Kategori
@endsection
@section('content')
    <a href="/category/create" class="btn btn-primary">Tambah</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($categories as $key => $value)
            <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{ $value->name }}</td>
                <td>
                    <form action="/category/{{ $value->id }}" method="POST">
                        <a href="/category/{{ $value->id }}" class="btn btn-sm btn-info">Detail</a>
                        <a href="/category/{{ $value->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                    </form>
                </td>
            </tr>
            @empty
                <p>Tidak Ada Category</p>
            @endforelse
        </tbody>
    </table>
@endsection
