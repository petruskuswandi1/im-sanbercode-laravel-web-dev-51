@extends('layouts.master')
@section('title')
    Halaman Detail Kategory
@endsection
@section('content')
    <h1>{{ $category->name }}</h1>
    <p>{{ $category->description }}</p>
@endsection
