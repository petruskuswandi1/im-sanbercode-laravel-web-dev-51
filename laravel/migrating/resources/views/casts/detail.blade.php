@extends('layouts.master')
@section('title')
    Halaman Detail Cast
@endsection
@section('content')
    <h1>{{ $cast->name }}</h1>
    <h5>{{ $cast->age }} Years</h5>
    <p>{{ $cast->bio }}</p>
@endsection
