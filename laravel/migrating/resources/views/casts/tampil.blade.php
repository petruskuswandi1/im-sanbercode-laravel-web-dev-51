@extends('layouts.master')
@section('title')
    Halaman Tampil Cast
@endsection
@section('content')
    <a href="/cast/create" class="btn btn-primary">Tambah</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $value)
            <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{ $value->name }}</td>
                <td>{{ $value->age }}</td>
                <td>
                    <form action="/cast/{{ $value->id }}" method="POST">
                        <a href="/cast/{{ $value->id }}" class="btn btn-sm btn-info">Detail</a>
                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                    </form>
                </td>
            </tr>
            @empty
                <p>Tidak Ada Cast</p>
            @endforelse
        </tbody>
    </table>
@endsection
