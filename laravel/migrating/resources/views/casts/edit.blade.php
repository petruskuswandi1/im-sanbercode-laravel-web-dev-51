@extends('layouts.master')
@section('title')
    Halaman Update Cast
@endsection
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Cast Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $cast->name }}">
        </div>
        <div class="form-group">
            <label for="age">Cast Age</label>
            <input type="number" name="age" id="age" class="form-control" value="{{ $cast->age }}">
        </div>
        <div class="form-group">
            <label for="bio">Cast Bio</label>
            <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{ $cast->bio }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
