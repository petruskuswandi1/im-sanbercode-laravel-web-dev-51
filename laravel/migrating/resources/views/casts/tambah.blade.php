@extends('layouts.master')
@section('title')
    Halaman Tambah Cast
@endsection
@section('content')
@if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $err)
                <li>{{ $err }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Cast Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="age">Cast Age</label>
            <input type="number" name="age" id="age" class="form-control">
        </div>
        <div class="form-group">
            <label for="bio">Cast Bio</label>
            <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
