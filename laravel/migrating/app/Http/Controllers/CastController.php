<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('casts.tambah');
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => 'required|min:3|max:45',
            'age' => 'required|min:2|max:11',
            'bio' => 'required|min:3',
        ]);

        DB::table('casts')->insert([
            'name' => $req->input('name'),
            'age' => $req->input('age'),
            'bio' => $req->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $casts = DB::table('casts')->get();

        return view('casts.tampil', ['casts' => $casts]);
    }

    public function show($id)
    {
        $cast = DB::table('casts')->find($id);

        return view('casts.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->find($id);

        return view('casts.edit', ['cast' => $cast]);
    }

    public function update(Request $req, $id)
    {
        $req->validate([
            'name' => 'required|min:3|max:45',
            'age' => 'required|min:2|max:11',
            'bio' => 'required|min:10',
        ]);

        DB::table('casts')->where('id', $id)->update([
            'name' => $req->input('name'),
            'age' => $req->input('age'),
            'bio' => $req->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
