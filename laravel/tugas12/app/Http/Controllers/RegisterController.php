<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $req)
    {
        $first_name = $req->input('fname');
        $last_name = $req->input('lname');

        return view('welcome', ["firstName" => $first_name, "lastName" => $last_name]);
    }
}
