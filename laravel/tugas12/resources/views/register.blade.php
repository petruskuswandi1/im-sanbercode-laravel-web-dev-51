<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First Name:</label><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label for="lname">Last Name:</label><br>
        <input type="text" name="lname" id="lname"><br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" name="gender" id="man"><label for="man">Man</label><br>
        <input type="radio" name="gender" id="woman"><label for="woman">Woman</label><br>
        <input type="radio" name="gender" id="other"><label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="jepang">Jepang</option>
            <option value="china">China</option>
        </select><br><br>
        <label for="language-spoken">Language Spoken:</label><br>
        <input type="checkbox" name="bhsIndonesia" id="bhsIndonesia" value="Bahasa Indonesia"><label for="bhsIndonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="bhsEnglish" id="bhsEnglish" value="English"><label for="bhsEnglish">English</label><br>
        <input type="checkbox" name="bhsArabic" id="bhsArabic" value="Arabic"><label for="bhsArabic">Arabic</label><br>
        <input type="checkbox" name="bhsJapanese" id="bhsJapanese" value="Japanese"><label for="bhsJapanese">Japanese</label><br><br>
        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
